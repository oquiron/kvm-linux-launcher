#!/bin/bash
# Oscar Pérez 2022
# v0.1 2022-10-16
# Instancia una VM de debian a partir de cloud.debian.org

# Limitación:
# --filemystem no funciona para /var/lib/containers
# libvirt se ejecuta con el usuario libvirt-qemu, por lo que el sistema de archivos
# debe pernetecer a ese usuario para que pueda grabar en el.

# El modo de seguridad se debe cambiar:
# https://libvirt.org/formatdomain.html#filesystems
# The filesystem element has an optional attribute accessmode which specifies the security mode for accessing the source (since 0.8.5) . Currently this only works with type='mount' for the QEMU/KVM driver. For driver type virtiofs, only passthrough is supported. For other driver types, the possible values are:
# man virt-install
#     accessmode or mode
#              The access mode for the source directory from the guest  OS.  Only  used  with  QEMU  and
#              type=mount.  Valid modes are 'passthrough' (the default), 'mapped', or 'squash'. See lib‐
#              virt domain XML documentation for more info.

# Si no uso ningún argumento funciona con passthrough

# Como rodeo, se agregó el parámetro 'rw' a este guión
# se puede montar con escrutura parcial, poro sigue sin funcionar podman /var/lib/containers


# TODO: Paramettrizar esto:
virsh net-start default

cat << EOF > /dev/null
# Este es el fragmento que debe incluirse en el cloud-init
bootcmd:
  # monta los archivos exportados por virt-install --filesystem
  # utiliza el mismo nombre de la etiqueta (tag) para montar el sistema de archivos
  # https://wiki.qemu.org/Documentation/9psetup
  # Multiples líneas se convierten en una sola, usar ; al final de cada comando bash
  - cat /sys/module/9pnet/holders/9pnet_virtio/drivers/virtio:9pnet_virtio/virtio*/mount_tag
        | tr '\0' '\n'
        | while read origen ; do
            echo $origen $origen 9p defaults 0 0 >> /etc/fstab ;
            echo $origen agregado a /etc/fstab ;
        done
  - mount -a

EOF

set +x

# valores por omisión, si no se especifica un argumento
ram=1024
cpus=1
descargar=false

for arg in "$@" ; do

    case ${arg} in
        --init=* )
            cloud_ini=${arg#*=}
            if [ ! -f ${cloud_ini} ] ; then
                echo "No existe el archivo ${cloud_ini}"
                exit
            fi
            ;;

        --descargar | --download )
            descargar=true
            ;;

        --memory=* | --ram=* )
            ram=${arg#*=}
            ;;

        --cpus=* )
            cpus=${arg#*=}
            ;;

        --size=* )
            size=${arg#*=}
            ;;

        --filesystem=* )
            # verifica que  tenga 3 valores
            if [ "$( echo $arg | sed 's/[^,]//g' | wc -c )" != 3 ] ; then
                echo
                echo "ERROR: incorrecto número de valores en $arg"
                echo "ejemplo: --filesystem=/origen,/destino,rw"
                echo "NOTA: rw implica: chown libvirt-qemu:libvirt-qemu /origen"
                echo
                exit
            fi


     # agrega el parámetro a un arreglo
            arg_fs=${arg%,*} # se elimina el tercer argumento
            filesystem_arg+=( ${arg_fs},accessmode=mapped )

            # extrae los valores
            modo=${arg#*,*,}
            tmp=${arg_fs#*=}
            origen=${tmp%,*}

            #echo origen=$origen, modo=$modo

            # verifica que el directorio origen exista
            if [ ! -d "${origen}" ] ; then
                echo "ERROR: No existe el directorio origen $origen"
                exit
            fi

            # otorga permisos sobre el origen
            if [ "${modo}" == "rw" ] ; then
                echo
                echo "PRECAUCIÓN: se cambio el dueño de ${origen} a libvirt-qemu:libvirt-qemu"
                ls -dl $origen
                # https://www.linux-kvm.org/page/9p_virtio
                chown libvirt-qemu:libvirt-qemu ${origen}
                ls -dl $origen
            fi

            #debug#
            #echo ${filesystem_arg[*]}
            ;;

        --* )
            echo "Opción desconocida ${arg}"
            exit
            ;;
    esac

done
echo
echo ${cloud_ini}, ram=$ram, cpus = $cpus



if [ "$cloud_ini" == ""  ] ; then
    echo "USO: $0 --init=cloud_ini.yaml [--descargar] [--memory=1024] [--cpus=1] [--filesistem=/srv/kvm/maquina/home,/home,ro] [--size=10G]"
    echo " Crea una MV a partir de una imagen de cloud.debian.org usando virt-install"
    echo "  --filesystem puede ser especificado multiples veces"
    echo "      El sistema de archivos 9p debe ser montado automáticamente por cloud-init"
    echo "      el segundo valor después de la coma, debe ser el directorio donde se montará el sistema de archivos"
    echo "      NOTA: rw cambia el dueño de /origen a libvirt-qemu:libvirt-qemu"
    echo "  --descargar utiliza rsync para descargar la última versión disponible en cloud.debian.org"
    echo "      si no se especifica, se utiliza la ultima versión descargada en /var/lib/libvirt/images/"
    echo "  --init El hostname se extrae del archivo cloud-init"
    echo "      este parámetro es obligatorio, ya que debe configurar las credenciales"
    echo "  --size Tamaño del disco duro"

    echo " Limitaciones: "
    echo "      - descarga solo la versión debian-11-generic-amd64-*.qcow2 "
    echo "      - no acepta parámetros extra para virt-install"
    echo "      - no se puede acceder rw a sistemas de archivos  9p "
    exit
fi



# extrae el nombre de la MV del archivo cloud-init.cfg

new_machine=$(grep "^hostname: " ${cloud_ini} | cut -f2 -d' ')


IMAGES_DIR=/var/lib/libvirt/images/
CODE_NAME=bookworm
OS_TYPE=generic
VERSION=12

if $descargar ; then

    echo "Consultando la ultima imagen Debian disponible en cloud.debian.org"
    ultimo=$(rsync rsync://cloud.debian.org/cdimage/cloud/${CODE_NAME}/ |  tail -2 | head -1 | sed 's/.* //')
    echo "versión [ ${ultimo} ] encontrada"

    # genericcloud no contiene controladores, no instancia la tarjeta de red
    imagen_nombre=debian-${VERSION}-generic-amd64-${ultimo}.qcow2


    # Descargar
    echo "Descargando ${imagen_nombre}"
    rsync -P rsync://cloud.debian.org/cdimage/cloud/${CODE_NAME}/${ultimo}/${imagen_nombre} \
    	${IMAGES_DIR}/${imagen_nombre}
    rsync -P rsync://cloud.debian.org/cdimage/cloud/${CODE_NAME}/${ultimo}/SHA512SUMS \
    	${IMAGES_DIR}/SHA512SUMS


    grep ${imagen_nombre} ${IMAGES_DIR}/SHA512SUMS > ${IMAGES_DIR}/${imagen_nombre}.sha512

    echo "Comprobando SHA512"
    cd ${IMAGES_DIR}/
    sha512sum -c ${IMAGES_DIR}/${imagen_nombre}.sha512
    cd -

else
    imagen_nombre=$(ls ${IMAGES_DIR}/debian-${VERSION}-generic-amd64-*.qcow2 2> /dev/null | tail -1 )
    if [ "${imagen_nombre}" == "" ] ; then
        echo "No se encontró ninguna imagen descargada"
        exit
    fi
    imagen_nombre=$(basename ${imagen_nombre} )
fi

if [ "${size}" = "" ] ; then
    size=10G
fi


# Instanciar una máquina KVM
# Crea un nueva imagen de disco
cp ${IMAGES_DIR}/${imagen_nombre} ${IMAGES_DIR}/${new_machine}.qcow2
chmod u+rw                        ${IMAGES_DIR}/${new_machine}.qcow2
chown libvirt-qemu:libvirt-qemu   ${IMAGES_DIR}/${new_machine}.qcow2

qemu-img resize                   ${IMAGES_DIR}/${new_machine}.qcow2 ${size}



virt-install \
    --name ${new_machine} \
    --import \
    --ram ${ram} --vcpus=${cpus} \
    --os-type=generic\
    --nographics  --noautoconsole \
    --network network=default \
    --disk ${IMAGES_DIR}/${new_machine}.qcow2 \
    --cloud-init user-data=${cloud_ini} \
    ${filesystem_arg[*]}



#    --disk path=test1-seed.img,device=cdrom

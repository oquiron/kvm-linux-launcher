
# Compartir un directorio entre el anfitrión y el huesped

Linux y QEMU soportan el protocolo 9p para compartir una carpeta entre host y gest.
Hay dos formas de compartirlaz;
    - security_model=passthrough el huesped usa los permisos y usuario del sistema de archivos del anfitrión (un chown 1000 x se aplica en ambos ). por eso requiere qemu se ejecute como root.
    - security_model=mapped el husped almacena los permisos y usuarios en metadados xattr del sistema anfitrión. Qemu se ejecuta sin privilegios root.

Lo que me parece extraño es que debian por omisión ejecuta qemu sin privilegios y el modo por omision de 9p es passthrough

Por lo pronto:
    - No ejecutaré qemu con root:root
    - No montaré
    - OJO: Los directorios compartidos se cambiarán al usuario libvirt-qemu 

Si quiero usar ligados debo ejecutar qemu como root
Para la máquina de desarrollo quizá no es tan grave.. mmmm


 with security_model=mapped (a.k.a. security_model=mapped-xattr) 9p is not following symlinks on host. That's the expected behaviour.

There are 2 distinct models:

security_model=passthrough uses the ownership information, permissions and symlink info etc. directly from the host's file system. This model requires the qemu binary to run as high privileged user (i.e. root) though, because it needs to be able to change file ownership, permissions and so forth. For that reason this model is only appropriate for use cases where there is a certain trust to what the guest system is doing. E.g. this model is commonly used by kernel coders to build kernel/driver code on host and then test run it as guest.

For untrusted guests (i.e. most cases) it is recommended to use security_model=mapped-xattr instead. In this mode all ownership information, permissions, etc. are emulated (i.e. mapped) by storing them as extended attributes on top of files on host. This way the qemu binary can run with a regular (unprivileged) user and pretend to host it would have all rights to change ownership and permissions.

You can read more details about this in the 9p developer description about the 9p 'local' driver here:
https://wiki.qemu.org/Documentation/9p#9p_Filesystem_Drivers


# Cambiar la ejecución de qemu a roott

## Todo el sistema
https://libvirt.org/drvqemu.html#posix-users-groups

In the "system" instance, libvirt releases from 0.7.0 onwards allow control over the user/group that the QEMU virtual machines are run as. A build of libvirt with no configuration parameters set will still run QEMU processes as root:root. It is possible to change this default by using the --with-qemu-user=$USERNAME and --with-qemu-group=$GROUPNAME arguments to 'configure' during build. It is strongly recommended that vendors build with both of these arguments set to 'qemu'. Regardless of this build time default, administrators can set a per-host default setting in the /etc/libvirt/qemu.conf configuration file via the user=$USERNAME and group=$GROUPNAME parameters. When a non-root user or group is configured, the libvirt QEMU driver will change uid/gid to match immediately before executing the QEMU binary for a virtual machine.
## Una sola máquina



https://www.libvirt.org/kbase/qemu-passthrough-security.html#disabling-security-protection-per-vm


DAC - in the domain XML an <seclabel> element with the dac model can be added, configured with a user / group account of root to make QEMU run with full privileges.


https://libvirt.org/formatdomain.html#security-label

<seclabel type='dynamic' model='dac'/>


    <filesystem type='mount' accessmode='mapped'>
      <source dir='/home/quiron/2022/kaxtli/'/>
      <target dir='/root/kaxtli/'/>
      <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>
    </filesystem>

